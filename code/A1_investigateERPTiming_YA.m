%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

clear avgS1 avgS2

for id = 1:length(IDs)
    for indRun = 1:2
        
        if indRun == 1
            condEEG = 'stroop1';
        elseif indRun == 2
            condEEG = 'stroop2';
        end

        display(['processing ID ' IDs{id}]);

        %% load data

        tmp = [];
        tmp.clock = tic; % set tic for processing duration

        %data.(condEEG) = load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
        data.(condEEG) = load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
    end
    
    cfg = [];
    avgS1{id} = ft_timelockanalysis(cfg, data.stroop1.data);
    avgS2{id} = ft_timelockanalysis(cfg, data.stroop2.data);
    
end % ID

%% create grand average across subjects

cfg = [];
avgGrandS1 = ft_timelockgrandaverage(cfg, avgS1{:});

cfg = [];
avgGrandS2 = ft_timelockgrandaverage(cfg, avgS2{:});

%% save data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
save([pn.dataOut, 'A1_StroopERPs_YA.mat'], 'avg*');

%% load data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
load([pn.dataOut, 'A1_StroopERPs_YA.mat']);

%% plotting section

figure;
subplot(3,1,1);
    imagesc(avgGrandS1.time,1:64,avgGrandS1.avg)
    hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    title('Grand average Stroop pre')
subplot(3,1,2);
    imagesc(avgGrandS2.time,1:64,avgGrandS2.avg)
    hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    title('Grand average Stroop post')
subplot(3,1,3);
    imagesc(avgGrandS2.time,1:64,avgGrandS2.avg-avgGrandS1.avg)
    hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    title('Grand average Stroop post-pre')
% 
% figure;
% for indChan = 1:60
%     hold on; plot(avgGrandS2.time,nanmean(avgGrandS2.avg(indChan,:),1))
% end
% hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
% hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
% hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2)
% set(gca,'YDir','reverse')

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,[1,2]);
    plot(avgGrandS2.time,nanmean(avgGrandS1.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS2.time,nanmean(avgGrandS2.avg(1:21,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre'; 'Stroop Post'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Frontal Grand Average')
    xlim([-1 2.9])
subplot(2,3,[4,5]);
    plot(avgGrandS2.time,nanmean(avgGrandS1.avg(44:60,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS2.time,nanmean(avgGrandS2.avg(44:60,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre'; 'Stroop Post'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Posterior-occiptal Grand Average')
    xlim([-1 2.9])
subplot(2,3,3);
cfg = [];
cfg.xlim = [.3 1];
cfg.zlim = [-.5 .5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1); title('Stroop Pre .3-1')
subplot(2,3,6);
ft_topoplotER(cfg,avgGrandS2); title('Stroop Post .3-.1')

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'StroopPrePostComparison';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


figure;
cfg = [];
cfg.showlabels = 'yes'; 
cfg.fontsize = 12; 
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.xlim = [-.1 .5];
cfg.xlim = [-.1 2];
%cfg.baseline = [-0.2 0]; 
ft_multiplotER(cfg, avgGrandS1, avgGrandS2);

figure;
subplot(1,2,1);
cfg = [];
cfg.xlim = [0.2 0.4];
cfg.zlim = [-2 2];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1);
subplot(1,2,2);
ft_topoplotER(cfg,avgGrandS2);

figure;
subplot(1,2,1);
cfg = [];
cfg.xlim = [1.2 1.4];
cfg.zlim = [-5 5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1);
subplot(1,2,2);
ft_topoplotER(cfg,avgGrandS2);

figure;
subplot(1,2,1);
cfg = [];
cfg.xlim = [-.5 0];
cfg.zlim = [-2 2];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1);
subplot(1,2,2);
ft_topoplotER(cfg,avgGrandS2);

%% topographies Stroop Pre

h = figure('units','normalized','position',[.1 .1 .7 .7]);
cfg = [];
cfg.xlim = [-1 : 0.1 : 2.0];
%cfg.zlim = [-2e-13 2e-13];
cfg.layout = 'acticap-64ch-standard2.mat';
clf;
ft_topoplotER(cfg,avgGrandS1);

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'A_topographiesGrandAverageStroop1';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% topographies Stroop Post

h = figure('units','normalized','position',[.1 .1 .7 .7]);
cfg = [];
cfg.xlim = [-1 : 0.1 : 2.0];
%cfg.zlim = [-2e-13 2e-13];
cfg.layout = 'acticap-64ch-standard2.mat';
clf;
ft_topoplotER(cfg,avgGrandS2);

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'A_topographiesGrandAverageStroop2';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plotting section

id = 1;

    figure; 
    imagesc(avgS1{id}.time,1:64,avgS2{id}.avg)
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2)
    
    figure; 
    plot(avgS1{id}.time,nanmean(avgS2{id}.avg(44:60,:),1))
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2)
    
    figure;
    cfg = [];
    cfg.showlabels = 'yes'; 
    cfg.fontsize = 6; 
    cfg.layout = 'acticap-64ch-standard2.mat';
    %cfg.ylim = [-3e-13 3e-13];
    ft_multiplotER(cfg, avgS1{id}); 
    
    cfg = [];
    cfg.showlabels = 'no'; 
    cfg.fontsize = 6; 
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.baseline = [-0.2 0]; 
%     cfg.xlim = [-0.2 1.0]; 
%     cfg.ylim = [-3e-13 3e-13]; 
    ft_multiplotER(cfg, avgS1{id}, avgS2{id});
    
    figure; 
    subplot(1,3,1);
    cfg = [];
    cfg.xlim = [0.2 0.6];
    cfg.zlim = [-3 3];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.colorbar = 'yes';
    ft_topoplotER(cfg,avgS1{id});
    subplot(1,3,2);
    ft_topoplotER(cfg,avgS2{id});
    
    
%% individual subject plot
    
time = [.3 1];
% Scaling of the vertical axis for the plots below
ymax = 10; 
figure; 
for isub = 1:47
    subplot(6,8,isub)
    % use the rectangle to indicate the time range used later 
    rectangle('Position',[time(1) -ymax (time(2)-time(1)) 2*ymax],'FaceColor',[0.7 0.7 0.7]);
    hold on;
    % plot the lines in front of the rectangle
    plot(avgS1{isub}.time,nanmean(avgS1{isub}.avg(1:21,:),1));
    plot(avgS2{isub}.time,nanmean(avgS2{isub}.avg(1:21,:),1),'r');
    title(strcat('subject ',num2str(isub)))
    ylim([-ymax ymax])
    xlim([-1 4])
end 

%% statistics

cfg = [];
cfg.channel     = 'all';
cfg.latency     = [0.3 1];
cfg.avgovertime = 'yes';
cfg.parameter   = 'avg';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.alpha       = 0.05;
cfg.correctm    = 'no';
cfg.correcttail = 'prob';
cfg.numrandomization = 1000;
 
Nsub = 47;
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
 
stat = ft_timelockstatistics(cfg,avgGrandS1{:},avgGrandS2{:});
 
% make the plot
cfg = [];
cfg.style     = 'blank';
cfg.layout    = 'acticap-64ch-standard2.mat';
cfg.highlight = 'on';
cfg.highlightchannel = find(stat.mask);
cfg.comment   = 'no';
figure; ft_topoplotER(cfg, avgGrandS1)
title('Nonparametric: significant with cluster multiple comparison correction')


figure;
cfg = [];
cfg.zlim = [-5 5];
cfg.alpha = .05;
cfg.layout = 'acticap-64ch-standard2.mat';
ft_clusterplot(cfg,stat);

%% time-resolved statistics

cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = avgGrandS1.label;
neighbours              = ft_prepare_neighbours(cfg_neighb, avgGrandS1); % use original dataset
clear cfg_neighb;

cfg = [];
cfg.channel     = 'all';
cfg.neighbours  = neighbours;
cfg.latency     = [0.3 1];
cfg.avgovertime = 'yes';
cfg.parameter   = 'avg';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.alpha       = 0.05;
cfg.correctm    = 'cluster';
cfg.correcttail = 'prob';
cfg.numrandomization = 1000;
cfg.minnbchan        = 2; % minimal neighbouring channels
 
Nsub = 47;
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
 
stat = ft_timelockstatistics(cfg,avgS1{:},avgS2{:});

% make a plot
cfg = [];
cfg.highlightsymbolseries = ['*','*','.','.','.'];
cfg.layout    = 'acticap-64ch-standard2.mat';
cfg.contournum = 0;
cfg.markersymbol = '.';
cfg.alpha = .05;
cfg.parameter='stat';
cfg.zlim = [-5 5];
ft_clusterplot(cfg,stat);