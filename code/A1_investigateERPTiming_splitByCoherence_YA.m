%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

pn.behavior = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/B_TrialInfo/B_data/B_behavioralFiles/';

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

clear avgS1 avgS2

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);
    for indRun = 1:2
        conditions = {'stroop1'; 'stroop2'};
        %% load data
        data.(conditions{indRun}) = load([pn.dataIn, IDs{id}, '_', conditions{indRun}, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
        %% get congruency information
        load([pn.behavior, IDs{id}, '_S', num2str(indRun)]);
        %% split by congruency
        curCongruency = behavData.congruency(data.(conditions{indRun}).data.cfg.trials);
        data.(conditions{indRun}).congruent = data.(conditions{indRun}).data;
        data.(conditions{indRun}).congruent.trial = data.(conditions{indRun}).data.trial(curCongruency==1);
        data.(conditions{indRun}).congruent.time = data.(conditions{indRun}).data.time(curCongruency==1);
        data.(conditions{indRun}).incongruent = data.(conditions{indRun}).data;
        data.(conditions{indRun}).incongruent.trial = data.(conditions{indRun}).data.trial(curCongruency==0);
        data.(conditions{indRun}).incongruent.time = data.(conditions{indRun}).data.time(curCongruency==0);
    end
    cfg = [];
    avgS1_congruent{id}     = ft_timelockanalysis(cfg, data.stroop1.congruent);
    avgS1_incongruent{id}   = ft_timelockanalysis(cfg, data.stroop1.incongruent);
    avgS2_congruent{id}     = ft_timelockanalysis(cfg, data.stroop2.congruent);
    avgS2_incongruent{id}   = ft_timelockanalysis(cfg, data.stroop2.incongruent);
    clear data;
end % ID

%% create grand average across subjects

cfg = [];
avgGrandS1_con      = ft_timelockgrandaverage(cfg, avgS1_congruent{:});
avgGrandS1_incon    = ft_timelockgrandaverage(cfg, avgS1_incongruent{:});
avgGrandS2_con      = ft_timelockgrandaverage(cfg, avgS2_congruent{:});
avgGrandS2_incon    = ft_timelockgrandaverage(cfg, avgS2_incongruent{:});

%% save data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
save([pn.dataOut, 'A1_StroopERPsByCongr_YA.mat'], 'avg*');

%% load data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
load([pn.dataOut, 'A1_StroopERPsByCongr_YA.mat']);

% figure;
% subplot(3,1,1);
%     imagesc(avgGrandS1_con.time,1:64,avgGrandS1_con.avg-avgGrandS1_incon.avg)
%     hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     title('Grand average Stroop pre')
% subplot(3,1,2);
%     imagesc(avgGrandS1_con.time,1:64,avgGrandS2_con.avg-avgGrandS2_incon.avg)
%     hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     title('Grand average Stroop post')
% subplot(3,1,3);
%     imagesc(avgGrandS1_con.time,1:64,(avgGrandS2_con.avg-avgGrandS2_incon.avg)-(avgGrandS1_con.avg-avgGrandS1_incon.avg))
%     hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     title('Grand average Stroop post-pre')
%     
%     
% figure;
% subplot(3,1,1);
%     imagesc(avgGrandS1_con.time,1:64,avgGrandS2_con.avg-avgGrandS1_con.avg)
%     hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     title('Grand average Stroop pre')
% subplot(3,1,2);
%     imagesc(avgGrandS1_con.time,1:64,avgGrandS2_incon.avg-avgGrandS1_incon.avg)
%     hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     title('Grand average Stroop post')
% subplot(3,1,3);
%     imagesc(avgGrandS1_con.time,1:64,(avgGrandS2_incon.avg-avgGrandS1_incon.avg)-(avgGrandS2_con.avg-avgGrandS1_con.avg))
%     hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
%     hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
%     title('Grand average Stroop post-pre')

% figure;
% for indChan = 1:60
%     hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2.avg(indChan,:),1))
% end
% hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
% hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
% hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2)
% set(gca,'YDir','reverse')

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,[1,2]);
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_con.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_con.avg(1:21,:),1),'LineWidth', 2)
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_incon.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_incon.avg(1:21,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre Con'; 'Stroop Post Con'; 'Stroop Pre Incon'; 'Stroop Post Incon'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Frontal Grand Average')
    xlim([-1 2.9])
subplot(2,3,[4,5]);
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_con.avg(44:60,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_con.avg(44:60,:),1),'LineWidth', 2)
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_incon.avg(44:60,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_incon.avg(44:60,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre Con'; 'Stroop Post Con'; 'Stroop Pre Incon'; 'Stroop Post Incon'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Posterior-occiptal Grand Average')
    xlim([-1 2.9])
subplot(2,3,3);
cfg = [];
cfg.xlim = [.3 1];
cfg.zlim = [-.5 .5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1_con); title('Stroop Pre Con .3-1')
subplot(2,3,6);
ft_topoplotER(cfg,avgGrandS2_incon); title('Stroop Post Incon .3-.1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'StroopPrePostComparison_byCong_YA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% retain the subject-level ERP data

cfg = [];
cfg.keepindividual = 'yes';
avgGrandS1_con_individual = ft_timelockgrandaverage(cfg, avgS1_congruent{:});
avgGrandS1_incon_individual = ft_timelockgrandaverage(cfg, avgS1_incongruent{:});
avgGrandS2_con_individual = ft_timelockgrandaverage(cfg, avgS2_congruent{:});
avgGrandS2_incon_individual = ft_timelockgrandaverage(cfg, avgS2_incongruent{:});

%% baseline-correction for subject-level ERPs

cfg = [];
cfg.baseline     = [-1 0];
cfg.parameter   = 'individual';
[avgGrandS1_con_individual_bl] = ft_timelockbaseline(cfg, avgGrandS1_con_individual);
[avgGrandS1_incon_individual_bl] = ft_timelockbaseline(cfg, avgGrandS1_incon_individual);
[avgGrandS2_con_individual_bl] = ft_timelockbaseline(cfg, avgGrandS2_con_individual);
[avgGrandS2_incon_individual_bl] = ft_timelockbaseline(cfg, avgGrandS2_incon_individual);

%% check for inter-individual differences in Post-Pre

dataToPlot = avgGrandS2_incon_individual.individual-avgGrandS1_con_individual.individual;
dataToPlot = squeeze(nanmean(dataToPlot(:,1:21,:),2));
[~, sortInd] = sort(nanmean(dataToPlot(:, 500:1500),2), 'descend');

dataToPlot = avgGrandS2_incon_individual_bl.individual-avgGrandS1_con_individual_bl.individual;
dataToPlot = squeeze(nanmean(dataToPlot(:,1:21,:),2));
[~, sortInd] = sort(nanmean(dataToPlot(:, 500:1500),2), 'descend');

h = figure('units','normalized','position',[.1 .1 .7 .7]);
imagesc(avgGrandS1_con.time,[],dataToPlot(sortInd,:))
hold on; line([0,0],[0,50], 'Color', 'k','LineWidth', 2)
hold on; line([2,2],[0,50], 'Color', 'k','LineWidth', 2)
hold on; line([3,3],[0,50], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
title('Stroop Post-Pre Incongruent');
xlabel('Time'); ylabel('Subjects (sorted by difference magnitude upon stim on)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'IndividualERPDifferences_YA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% time-resolved statistics: MANOVA

cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = avgS1_congruent{1}.label;
neighbours              = ft_prepare_neighbours(cfg_neighb, avgS1_congruent{1}); % use original dataset
clear cfg_neighb;

cfg = [];
cfg.channel     = 'all';
cfg.neighbours  = neighbours;
cfg.latency     = [0.3 1];
cfg.avgovertime = 'no';
cfg.parameter   = 'avg';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesFmultivariate';
cfg.alpha       = 0.05;
cfg.tail        = 1;
cfg.correctm    = 'cluster';
%cfg.correcttail = 'prob';
cfg.numrandomization = 500;
cfg.minnbchan        = 2; % minimal neighbouring channels
 
Nsub = 47;
cfg.design(1,1:4*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub) 3*ones(1,Nsub) 4*ones(1,Nsub)];
cfg.design(2,1:4*Nsub)  = [1:Nsub 1:Nsub 1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
 
stat = ft_timelockstatistics(cfg,avgS1_congruent{:}, ...
    avgS1_incongruent{:}, avgS2_congruent{:}, ...
    avgS2_incongruent{:});

% make a plot
cfg = [];
cfg.highlightsymbolseries = ['*','*','.','.','.'];
cfg.layout    = 'acticap-64ch-standard2.mat';
cfg.contournum = 0;
cfg.markersymbol = '.';
cfg.alpha = .05;
cfg.parameter='stat';
%cfg.zlim = [-5 5];
ft_clusterplot(cfg,stat);

%% MANOVA: average over time

cfg = [];
cfg.channel     = 'all';
cfg.neighbours  = neighbours;
cfg.latency     = [0.3 1];
cfg.avgovertime = 'yes';
cfg.parameter   = 'avg';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesFmultivariate';
cfg.alpha       = 0.05;
cfg.tail        = 1;
cfg.correctm    = 'cluster';
%cfg.correcttail = 'prob';
cfg.numrandomization = 500;
cfg.minnbchan        = 2; % minimal neighbouring channels
 
Nsub = 47;
cfg.design(1,1:4*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub) 3*ones(1,Nsub) 4*ones(1,Nsub)];
cfg.design(2,1:4*Nsub)  = [1:Nsub 1:Nsub 1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
 
statAVG = ft_timelockstatistics(cfg,avgS1_congruent{:}, ...
    avgS1_incongruent{:}, avgS2_congruent{:}, ...
    avgS2_incongruent{:});

% make a plot
cfg = [];
cfg.highlightsymbolseries = ['*','*','.','.','.'];
cfg.layout    = 'acticap-64ch-standard2.mat';
cfg.contournum = 0;
cfg.markersymbol = '.';
cfg.alpha = .05;
cfg.parameter='stat';
cfg.subplotsize = [1 1];
%cfg.zlim = [-5 5];
ft_clusterplot(cfg,statAVG);

%% ANOVA: average over time

cfg = [];
cfg.channel     = 'all';
cfg.neighbours  = neighbours;
cfg.latency     = [0.3 1];
cfg.avgovertime = 'yes';
cfg.parameter   = 'avg';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesFunivariate';
cfg.alpha       = 0.05;
cfg.tail        = 1;
cfg.correctm    = 'cluster';
%cfg.correcttail = 'prob';
cfg.numrandomization = 500;
cfg.minnbchan        = 2; % minimal neighbouring channels
 
Nsub = 47;
cfg.design(1,1:4*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub) 3*ones(1,Nsub) 4*ones(1,Nsub)];
cfg.design(2,1:4*Nsub)  = [1:Nsub 1:Nsub 1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
 
statAVGUNI = ft_timelockstatistics(cfg,avgS1_congruent{:}, ...
    avgS1_incongruent{:}, avgS2_congruent{:}, ...
    avgS2_incongruent{:});

% no clusters detected

%% time-resolved statistics: ANOVA

cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = avgS1_congruent{1}.label;
neighbours              = ft_prepare_neighbours(cfg_neighb, avgS1_congruent{1}); % use original dataset
clear cfg_neighb;

cfg = [];
cfg.channel     = 'all';
cfg.neighbours  = neighbours;
cfg.latency     = [0.3 1];
cfg.avgovertime = 'no';
cfg.parameter   = 'avg';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesFunivariate';
cfg.alpha       = 0.05;
cfg.tail        = 1;
cfg.correctm    = 'cluster';
%cfg.correcttail = 'prob';
cfg.numrandomization = 500;
cfg.minnbchan        = 2; % minimal neighbouring channels
 
Nsub = 47;
cfg.design(1,1:4*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub) 3*ones(1,Nsub) 4*ones(1,Nsub)];
cfg.design(2,1:4*Nsub)  = [1:Nsub 1:Nsub 1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
 
statUNI = ft_timelockstatistics(cfg,avgS1_congruent{:}, ...
    avgS1_incongruent{:}, avgS2_congruent{:}, ...
    avgS2_incongruent{:});

% make a plot
cfg = [];
cfg.highlightsymbolseries = ['*','*','.','.','.'];
cfg.layout    = 'acticap-64ch-standard2.mat';
cfg.contournum = 0;
cfg.markersymbol = '.';
cfg.alpha = .05;
cfg.parameter='stat';
cfg.zlim = [-5 5];
ft_clusterplot(cfg,statUNI);