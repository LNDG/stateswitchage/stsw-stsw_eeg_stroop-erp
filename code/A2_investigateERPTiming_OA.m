%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 51; 
% 2160 removed as S2 is missing 
% 2112 removed due to bad data quality
IDs = {'2104';'2107';'2108';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

clear avgS1 avgS2

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);
    for indRun = 1:2
        conditions = {'stroop1'; 'stroop2'};
        %% load data
        data.(conditions{indRun}) = load([pn.dataIn, IDs{id}, '_', conditions{indRun}, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
    end
    cfg = [];
    avgS1{id} = ft_timelockanalysis(cfg, data.stroop1.data);
    avgS2{id} = ft_timelockanalysis(cfg, data.stroop2.data);
    clear data;
end % ID

%% check magnitude of individual ERPs (across channels)

figure;
for id = 1:length(IDs)
    hold on; plot(nanmean(avgS1{id}.avg,1));
end
    
%% create grand average across subjects

cfg = [];
cfg.keepindividual = 'no';
avgGrandS1 = ft_timelockgrandaverage(cfg, avgS1{:});

cfg = [];
cfg.keepindividual = 'no';
avgGrandS2 = ft_timelockgrandaverage(cfg, avgS2{:});

%% save data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
save([pn.dataOut, 'A1_StroopERPs_OA.mat'], 'avg*');

%% load data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
load([pn.dataOut, 'A1_StroopERPs_OA.mat']);

%% plot pre, post and difference by channel

figure;
subplot(3,1,1);
    imagesc(avgGrandS1.time,1:64,avgGrandS1.avg)
    hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    title('Grand average Stroop pre')
subplot(3,1,2);
    imagesc(avgGrandS2.time,1:64,avgGrandS2.avg)
    hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    title('Grand average Stroop post')
subplot(3,1,3);
    imagesc(avgGrandS2.time,1:64,avgGrandS2.avg-avgGrandS1.avg)
    hold on; line([-1,-1],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([2,2],[64,0], 'Color', 'w','LineWidth', 2)
    hold on; line([3,3],[64,0], 'Color', 'w','LineWidth', 2, 'LineStyle', '--')
    title('Grand average Stroop post-pre')

%% butterfly plot
    
figure;
for indChan = 1:60
    hold on; plot(avgGrandS2.time,nanmean(avgGrandS2.avg(indChan,:),1))
end
hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2)
set(gca,'YDir','reverse')

%% plot grand ERP with topography

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,[1,2]);
    plot(avgGrandS2.time,nanmean(avgGrandS1.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS2.time,nanmean(avgGrandS2.avg(1:21,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre'; 'Stroop Post'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Frontal Grand Average')
    xlim([-1 2.9])
subplot(2,3,[4,5]);
    plot(avgGrandS2.time,nanmean(avgGrandS1.avg(44:60,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS2.time,nanmean(avgGrandS2.avg(44:60,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre'; 'Stroop Post'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Posterior-occiptal Grand Average')
    xlim([-1 2.9])
subplot(2,3,3);
cfg = [];
cfg.xlim = [.3 1];
cfg.zlim = [-.5 .5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1); title('Stroop Pre .3-1')
subplot(2,3,6);
ft_topoplotER(cfg,avgGrandS2); title('Stroop Post .3-1')

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'StroopPrePostComparison_OA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot multiplot

figure;
cfg = [];
cfg.showlabels = 'yes'; 
cfg.fontsize = 12; 
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.xlim = [-.1 .5];
cfg.xlim = [-.1 2];
%cfg.baseline = [-0.2 0]; 
ft_multiplotER(cfg, avgGrandS1, avgGrandS2);

%% plot topographies for different intervals

figure;
subplot(1,2,1);
cfg = [];
cfg.xlim = [0.2 0.4];
cfg.zlim = [-2 2];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1);
subplot(1,2,2);
ft_topoplotER(cfg,avgGrandS2);

figure;
subplot(1,2,1);
cfg = [];
cfg.xlim = [1.2 1.4];
cfg.zlim = [-5 5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1);
subplot(1,2,2);
ft_topoplotER(cfg,avgGrandS2);

figure;
subplot(1,2,1);
cfg = [];
cfg.xlim = [-.5 0];
cfg.zlim = [-2 2];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1);
subplot(1,2,2);
ft_topoplotER(cfg,avgGrandS2);

%% plot time-resolved topoplots of ERP: Stroop Pre

h = figure('units','normalized','position',[.1 .1 .7 .7]);
cfg = [];
cfg.xlim = [-1 : 0.1 : 2.0];
%cfg.zlim = [-2e-13 2e-13];
cfg.layout = 'acticap-64ch-standard2.mat';
clf;
ft_topoplotER(cfg,avgGrandS1);

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'A_topographiesGrandAverageStroop1';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot time-resolved topoplots of ERP: Stroop Post

h = figure('units','normalized','position',[.1 .1 .7 .7]);
cfg = [];
cfg.xlim = [-1 : 0.1 : 2.0];
%cfg.zlim = [-2e-13 2e-13];
cfg.layout = 'acticap-64ch-standard2.mat';
clf;
ft_topoplotER(cfg,avgGrandS2);

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'A_topographiesGrandAverageStroop2';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
