%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

pn.behavior = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/B_TrialInfo/B_data/B_behavioralFiles/';

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 51; 
% 2160 removed as S2 is missing 
% 2112 removed due to bad data quality
IDs = {'2104';'2107';'2108';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

clear avgS1 avgS2

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);
    for indRun = 1:2
        conditions = {'stroop1'; 'stroop2'};
        %% load data
        data.(conditions{indRun}) = load([pn.dataIn, IDs{id}, '_', conditions{indRun}, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
        %% get congruency information
        load([pn.behavior, IDs{id}, '_S', num2str(indRun)]);
        %% split by congruency
        curCongruency = behavData.congruency(data.(conditions{indRun}).data.cfg.trials);
        data.(conditions{indRun}).congruent = data.(conditions{indRun}).data;
        data.(conditions{indRun}).congruent.trial = data.(conditions{indRun}).data.trial(curCongruency==1);
        data.(conditions{indRun}).congruent.time = data.(conditions{indRun}).data.time(curCongruency==1);
        data.(conditions{indRun}).incongruent = data.(conditions{indRun}).data;
        data.(conditions{indRun}).incongruent.trial = data.(conditions{indRun}).data.trial(curCongruency==0);
        data.(conditions{indRun}).incongruent.time = data.(conditions{indRun}).data.time(curCongruency==0);
    end
    cfg = [];
    avgS1_congruent{id}     = ft_timelockanalysis(cfg, data.stroop1.congruent);
    avgS1_incongruent{id}   = ft_timelockanalysis(cfg, data.stroop1.incongruent);
    avgS2_congruent{id}     = ft_timelockanalysis(cfg, data.stroop2.congruent);
    avgS2_incongruent{id}   = ft_timelockanalysis(cfg, data.stroop2.incongruent);
    clear data;
end % ID

%% create grand average across subjects

cfg = [];
avgGrandS1_con      = ft_timelockgrandaverage(cfg, avgS1_congruent{:});
avgGrandS1_incon    = ft_timelockgrandaverage(cfg, avgS1_incongruent{:});
avgGrandS2_con      = ft_timelockgrandaverage(cfg, avgS2_congruent{:});
avgGrandS2_incon    = ft_timelockgrandaverage(cfg, avgS2_incongruent{:});

%% save data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
save([pn.dataOut, 'A2_StroopERPsByCongr_OA.mat'], 'avg*');

%% load data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
load([pn.dataOut, 'A2_StroopERPsByCongr_OA.mat']);

%% plot data

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,[1,2]);
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_con.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_con.avg(1:21,:),1),'LineWidth', 2)
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_incon.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_incon.avg(1:21,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre Con'; 'Stroop Post Con'; 'Stroop Pre Incon'; 'Stroop Post Incon'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Frontal Grand Average')
    xlim([-1 2.9])
subplot(2,3,[4,5]);
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_con.avg(44:60,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_con.avg(44:60,:),1),'LineWidth', 2)
    plot(avgGrandS1_con.time,nanmean(avgGrandS1_incon.avg(44:60,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS1_con.time,nanmean(avgGrandS2_incon.avg(44:60,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([2,2],[-10,10], 'Color', 'k','LineWidth', 2)
    hold on; line([3,3],[-10,10], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')
    %set(gca,'YDir','reverse')
    legend({'Stroop Pre Con'; 'Stroop Post Con'; 'Stroop Pre Incon'; 'Stroop Post Incon'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Posterior-occiptal Grand Average')
    xlim([-1 2.9])
subplot(2,3,3);
cfg = [];
cfg.xlim = [.3 1];
cfg.zlim = [-.5 .5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1_con); title('Stroop Pre Con .3-1')
subplot(2,3,6);
ft_topoplotER(cfg,avgGrandS2_incon); title('Stroop Post Incon .3-.1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'StroopPrePostComparison_byCong_OA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% check for inter-individual differences in Post-Pre

cfg = [];
cfg.keepindividual = 'yes';
avgGrandS1_con_individual = ft_timelockgrandaverage(cfg, avgS1_congruent{[1:3, 5:6,8:31, 33:end]});

cfg = [];
cfg.keepindividual = 'yes';
avgGrandS1_incon_individual = ft_timelockgrandaverage(cfg, avgS1_incongruent{[1:3, 5:6,8:31, 33:end]});

cfg = [];
cfg.keepindividual = 'yes';
avgGrandS2_con_individual = ft_timelockgrandaverage(cfg, avgS2_congruent{[1:3, 5:6,8:31, 33:end]});

cfg = [];
cfg.keepindividual = 'yes';
avgGrandS2_incon_individual = ft_timelockgrandaverage(cfg, avgS2_incongruent{[1:3, 5:6,8:31, 33:end]});


dataToPlot = avgGrandS2_incon_individual.individual-avgGrandS1_incon_individual.individual;
dataToPlot = squeeze(nanmean(dataToPlot(:,1:21,:),2));
[~, sortInd] = sort(nanmean(dataToPlot(:, 500:1500),2), 'descend');

h = figure('units','normalized','position',[.1 .1 .7 .7]);
imagesc(avgGrandS1_con.time,[],dataToPlot(sortInd,:))
hold on; line([0,0],[0,50], 'Color', 'k','LineWidth', 2)
hold on; line([2,2],[0,50], 'Color', 'k','LineWidth', 2)
hold on; line([3,3],[0,50], 'Color', 'k','LineWidth', 2, 'LineStyle', '--')

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'IndividualERPDifferences_OA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
