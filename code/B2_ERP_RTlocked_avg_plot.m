%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.tools        = fullfile(rootpath, '..', 'tfr', 'tools');
pn.fieldtrip    = fullfile(pn.tools, 'fieldtrip'); 
    addpath(pn.fieldtrip); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'))
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
pn.figures = fullfile(rootpath, 'figures');

pn.dataOut = fullfile(rootpath, 'data');
pn.sanityOut = fullfile(rootpath, '..', '..', 'stsw_beh_stroop', 'data');

%% load data

YAdata = load(fullfile(pn.dataOut, 'B_StroopERPs_RT_YA.mat'));
OAdata = load(fullfile(pn.dataOut, 'B_StroopERPs_RT_OA.mat'));

for indSession = 1:2
    for indID = 1:size(YAdata.avg_i0,2)
        indGroup = 1;
        indInterference = 1;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = YAdata.avg_i0{indSession,indID}.avg;
        indInterference = 2;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = YAdata.avg_i1{indSession,indID}.avg;
    end
    for indID = 1:size(OAdata.avg_i0,2)
        indGroup = 2;
        indInterference = 1;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = OAdata.avg_i0{indSession,indID}.avg;
        indInterference = 2;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = OAdata.avg_i1{indSession,indID}.avg;
    end
end

time = OAdata.avgGrandS2_c0.time;
label = OAdata.avgGrandS2_c0.label;

%% Overview Figure

load(fullfile(pn.sanityOut, 'E_audioRTlocked.mat'), 'AudioRTlocked')

cBrew = brewermap(4,'RdBu');

h = figure('units','normalized','position',[.1 .1 .20 .25]);
set(gcf,'renderer','Painters')
    cla; hold on;
    
    ERPs_all = cat(3,ERPdata{1},ERPdata{2});
    
    smoothPoints = 15; % 30 ms smoothing
    yyaxis left;
    curData = squeeze(nanmean(nanmean(ERPs_all(:,:,:,54,:),2),1));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h1 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(1,:)],'linewidth', 1,...
        'linestyle', '-', 'marker', 'none'}, 'patchSaturation', .25);
  
    hold on; line([0,0],[-2,7], 'Color', 'k','LineWidth', 1, 'LineStyle', ':')
%     legend([h1.mainLine, h3.mainLine], ...
%         {'YA'; 'OA'}, 'orientation', 'horizontal')
    xlabel('Time (peri-SOT; s)'); ylabel('Amplitude')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('POz (CPP)')
    xlim([-.9 .6]); ylim([-3 6]);
    
    yyaxis right;
    timeVoice = -1:1/44100:1;
    plotData = nanmean(AudioRTlocked,2);
    plotData = smoothts(squeeze(plotData),'b',440);
    plotData = zscore(plotData,[],2);
    plotData = plotData(:,1:44100/4410:end);
    timeVoice = timeVoice(1:44100/4410:end);
    %plot(time, squeeze(nanmean(zscore(plotData,[],2),1)), 'Color', [.6 .6 .6], 'LineWidth',1.5)
    standError = nanstd(plotData,1)./sqrt(size(plotData,1));
    %plot(timeVoice,nanmean(plotData,1))
    shadedErrorBar(timeVoice,nanmean(plotData,1),standError, ...
        'lineprops', {'color', [.6 .6 .6],'linewidth', 2}, 'patchSaturation', .25);
    ylabel('Speech signal power (z-score)')
    ylim([-.05 .3]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'B_StroopCPP_YAOA_avg';
%print(h,fullfile(pn.figures, figureName),'-depsc');
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'svg');


h = figure('units','normalized','position',[.1 .1 .20 .25]);
set(gcf,'renderer','Painters')
    % plot CPP topography across groups
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'South';
    cfg.zlim = [-3.5 3.5];
    cfg.colormap = cBrew;
    cfg.figure = h;
    plotData = [];
    plotData.label = label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    idxTime = time>-.300 & time<0;
    grandAvg = [squeeze(nanmean(nanmean(nanmean(nanmean(ERPdata{1}(:,:,:,1:60,idxTime),5),2),1),3)),...
        squeeze(nanmean(nanmean(nanmean(nanmean(ERPdata{2}(:,:,:,1:60,idxTime),5),2),1),3))];
    plotData.powspctrm = squeeze(nanmean(grandAvg,2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); %set(get(cb,'ylabel'),'string','ERP amplitude');
    title('300 ms pre-SOT')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
figureName = 'B_StroopCPP_YAOA_topo';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'svg');

%% plot separately for export (eps error when importing in Illustrator)
% 
% h = figure('units','normalized','position',[.1 .1 .5 .25]);
% set(gcf,'renderer','Painters')
% subplot(1,4,[1:3])
%     cla; hold on;
%     
%     timeVoice = -1:1/44100:1;
%     plotData = nanmean(AudioRTlocked,2);
%     plotData = smoothts(squeeze(plotData),'b',440);
%     plotData = zscore(plotData,[],2);
%     %plotData = smoothts(squeeze(plotData),'b',440);
%     % downsample
%     plotData = plotData(:,1:44100/4410:end);
%     timeVoice = timeVoice(1:44100/4410:end);
%     %plot(time, squeeze(nanmean(zscore(plotData,[],2),1)), 'Color', [.6 .6 .6], 'LineWidth',1.5)
%     standError = nanstd(plotData,1)./sqrt(size(plotData,1));
%     %plot(timeVoice,nanmean(plotData,1))
%     shadedErrorBar(timeVoice,nanmean(plotData,1),standError, ...
%         'lineprops', {'color', [.6 .6 .6],'linewidth', 1}, 'patchSaturation', .25);
%     ylabel('Speech signal power (z-score)')
%     xlim([-.8 .8]); ylim([0 .3]);
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% figureName = 'B_StroopCPP_YAOA_overlay';
% saveas(h, fullfile(pn.figures, figureName), 'eps');
% saveas(h, fullfile(pn.figures, figureName), 'png');