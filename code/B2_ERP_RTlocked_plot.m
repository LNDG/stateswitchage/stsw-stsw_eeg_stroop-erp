%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.tools        = fullfile(rootpath, '..', 'tfr', 'tools');
pn.fieldtrip    = fullfile(pn.tools, 'fieldtrip'); 
    addpath(pn.fieldtrip); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'))
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
pn.figures = fullfile(rootpath, 'figures');

pn.dataOut = fullfile(rootpath, 'data');
pn.sanityOut = fullfile(rootpath, '..', '..', 'stsw_beh_stroop', 'data');

%% load data

YAdata = load(fullfile(pn.dataOut, 'B_StroopERPs_RT_YA.mat'));
OAdata = load(fullfile(pn.dataOut, 'B_StroopERPs_RT_OA.mat'));

for indSession = 1:2
    for indID = 1:size(YAdata.avg_i0,2)
        indGroup = 1;
        indInterference = 1;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = YAdata.avg_i0{indSession,indID}.avg;
        indInterference = 2;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = YAdata.avg_i1{indSession,indID}.avg;
    end
    for indID = 1:size(OAdata.avg_i0,2)
        indGroup = 2;
        indInterference = 1;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = OAdata.avg_i0{indSession,indID}.avg;
        indInterference = 2;
        ERPdata{indGroup}(indInterference,indSession,indID,:,:) = OAdata.avg_i1{indSession,indID}.avg;
    end
end

time = OAdata.avgGrandS2_c0.time;
label = OAdata.avgGrandS2_c0.label;

%% Overview Figure

% overlay SOT
% add error bars?

% h = figure('units','normalized','position',[.1 .1 .3 .3]);
% subplot(2,2,[1,2])
%     cla;
%     curData = squeeze(nanmean(ERPdata{1}(1,:,:,54,:),2));
%     h1 = plot(time,nanmean(curData,1), 'k-', 'LineWidth', 2);
%     curData = squeeze(nanmean(ERPdata{1}(2,:,:,54,:),2));
%     hold on; h2 = plot(time,nanmean(curData,1),'k--','LineWidth', 2)
%     curData = squeeze(nanmean(ERPdata{2}(1,:,:,54,:),2));
%     hold on; h3 = plot(time,nanmean(curData,1), 'r-', 'LineWidth', 2);
%     curData = squeeze(nanmean(ERPdata{2}(2,:,:,54,:),2));
%     hold on; h4 = plot(time,nanmean(curData,1),'r--','LineWidth', 2)
%     hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 1, 'LineStyle', ':')
%     legend([h1, h2, h3, h4], {'Match YA'; 'Mismatch YA';'Match OA'; 'Mismatch OA'}, 'orientation', 'horizontal')
%     xlabel('Time (peri-SOT; s)'); ylabel('Amplitude')
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     legend('boxoff')
%     title('POz (CPP)')
%     xlim([-1 1]); ylim([-3 9.5]);
% subplot(2,2,3);
%     % plot CPP topography for YA
%     addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
%     cBrew = brewermap(500,'RdBu');
%     cBrew = flipud(cBrew);
%     cfg = [];
%     cfg.layout = 'acticap-64ch-standard2.mat';
%     cfg.parameter = 'powspctrm';
%     cfg.comment = 'no';
%     cfg.colorbar = 'EastOutside';
%     %cfg.zlim = [-3*10^-4 6*10^-4];
%     cfg.colormap = cBrew;
%     plotData = [];
%     plotData.label = label(1:60); % {1 x N}
%     plotData.dimord = 'chan';
%     idxTime = time>-.300 & time<0;
%     grandAvg = squeeze(nanmean(nanmean(nanmean(nanmean(ERPdata{1}(:,:,:,1:60,idxTime),5),2),1),3));
%     plotData.powspctrm = grandAvg;
%     ft_topoplotER(cfg,plotData);
%     cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude');
% subplot(2,2,4);
%     % plot CPP topography for OA
%     grandAvg = squeeze(nanmean(nanmean(nanmean(nanmean(ERPdata{2}(:,:,:,1:60,idxTime),5),2),1),3));
%     plotData.powspctrm = grandAvg;
%     ft_topoplotER(cfg,plotData);
%     cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude');
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)

load(fullfile(pn.sanityOut, 'E_audioRTlocked.mat'), 'AudioRTlocked')

cBrew = brewermap(4,'RdBu');

h = figure('units','normalized','position',[.1 .1 .5 .25]);
set(gcf,'renderer','Painters')
subplot(1,4,[1:3])
    cla; hold on;
    
    smoothPoints = 15; % 30 ms smoothing
    yyaxis left;
    curData = squeeze(nanmean(ERPdata{1}(1,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h1 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(1,:)],'linewidth', 1,...
        'linestyle', '-', 'marker', 'none'}, 'patchSaturation', .25);
    %h1 = plot(time,nanmean(curData,1), 'k-', 'LineWidth', 2);
    curData = squeeze(nanmean(ERPdata{1}(2,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h2 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(1,:)],'linewidth', 1, ...
        'linestyle', '--', 'marker', 'none'}, 'patchSaturation', .25);
    %hold on; h2 = plot(time,nanmean(curData,1),'k--','LineWidth', 2)
    curData = squeeze(nanmean(ERPdata{2}(1,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h3 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(4,:)],'linewidth', 1,...
        'linestyle', '-', 'marker', 'none'}, 'patchSaturation', .25);
    %hold on; h3 = plot(time,nanmean(curData,1), 'r-', 'LineWidth', 2);
    curData = squeeze(nanmean(ERPdata{2}(2,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h4 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(4,:)],'linewidth', 1, ...
        'linestyle', '--', 'marker', 'none'}, 'patchSaturation', .25);
    %hold on; h4 = plot(time,nanmean(curData,1),'r--','LineWidth', 2)
    hold on; line([0,0],[-2,7], 'Color', 'k','LineWidth', 1, 'LineStyle', ':')
    legend([h1.mainLine, h2.mainLine, h3.mainLine, h4.mainLine], ...
        {'Match YA'; 'Mismatch YA';'Match OA'; 'Mismatch OA'}, 'orientation', 'horizontal')
    xlabel('Time (peri-SOT; s)'); ylabel('Amplitude')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('POz (CPP)')
    xlim([-.8 .8]); ylim([-3 9.5]);
    
    yyaxis right;
    timeVoice = -1:1/44100:1;
    plotData = nanmean(AudioRTlocked,2);
    plotData = smoothts(squeeze(plotData),'b',440);
    plotData = zscore(plotData,[],2);
    plotData = plotData(:,1:44100/4410:end);
    timeVoice = timeVoice(1:44100/4410:end);
    %plot(time, squeeze(nanmean(zscore(plotData,[],2),1)), 'Color', [.6 .6 .6], 'LineWidth',1.5)
    standError = nanstd(plotData,1)./sqrt(size(plotData,1));
    %plot(timeVoice,nanmean(plotData,1))
    shadedErrorBar(timeVoice,nanmean(plotData,1),standError, ...
        'lineprops', {'color', [.6 .6 .6],'linewidth', 1}, 'patchSaturation', .25);
    ylabel('Speech signal power (z-score)')
    ylim([0 .3]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    subplot(1,4,[4])
    % plot CPP topography across groups
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'South';
    cfg.zlim = [-3.5 3.5];
    cfg.colormap = cBrew;
    plotData = [];
    plotData.label = label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    idxTime = time>-.300 & time<0;
    grandAvg = [squeeze(nanmean(nanmean(nanmean(nanmean(ERPdata{1}(:,:,:,1:60,idxTime),5),2),1),3)),...
        squeeze(nanmean(nanmean(nanmean(nanmean(ERPdata{2}(:,:,:,1:60,idxTime),5),2),1),3))];
    plotData.powspctrm = squeeze(nanmean(grandAvg,2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); %set(get(cb,'ylabel'),'string','ERP amplitude');
    title('300 ms pre-SOT')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
figureName = 'B_StroopCPP_YAOA';
%saveas(h, fullfile(pn.figures, figureName), 'eps');
print(h,fullfile(pn.figures, figureName),'-depsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'svg');

%% plot separately for export (eps error when importing in Illustrator)

h = figure('units','normalized','position',[.1 .1 .5 .25]);
set(gcf,'renderer','Painters')
subplot(1,4,[1:3])
    cla; hold on;
    
    timeVoice = -1:1/44100:1;
    plotData = nanmean(AudioRTlocked,2);
    plotData = smoothts(squeeze(plotData),'b',440);
    plotData = zscore(plotData,[],2);
    %plotData = smoothts(squeeze(plotData),'b',440);
    % downsample
    plotData = plotData(:,1:44100/4410:end);
    timeVoice = timeVoice(1:44100/4410:end);
    %plot(time, squeeze(nanmean(zscore(plotData,[],2),1)), 'Color', [.6 .6 .6], 'LineWidth',1.5)
    standError = nanstd(plotData,1)./sqrt(size(plotData,1));
    %plot(timeVoice,nanmean(plotData,1))
    shadedErrorBar(timeVoice,nanmean(plotData,1),standError, ...
        'lineprops', {'color', [.6 .6 .6],'linewidth', 1}, 'patchSaturation', .25);
    ylabel('Speech signal power (z-score)')
    xlim([-.8 .8]); ylim([0 .3]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'B_StroopCPP_YAOA_overlay';
saveas(h, fullfile(pn.figures, figureName), 'eps');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% extract CPP slopes

dataForSlopeFit = []; slopes = [];
for indAge = 1:2
    for indCond = 1:2
        curData = squeeze(nanmean(ERPdata{indAge}(indCond,1:2,:,:,:),2));
        dataForSlopeFit{indAge}(:,indCond,:) = squeeze(curData(:,54,time>-.600 & time<-.100));
        for indID = 1:size(dataForSlopeFit{indAge},1)
            slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
        end
    end
end

h = figure('units','centimeters','position',[.1 .1 20 9]);
set(gcf,'renderer','Painters')
for indGroup = 1:2
    subplot(1,2,indGroup)
    box off;
        hold on;
        cBrew(1,:) = cBrew(4,:);
        cBrew(2,:) = cBrew(4,:);

        dataToPlot = squeeze(slopes{indGroup}(:,:,1));
                
        idx_outlier = cell(1); idx_standard = cell(1);
        % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
        X = [1 1; 1 2]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
        outliers = isoutlier(IndividualSlopes, 'median');
        idx_outlier{indGroup} = find(outliers);
        idx_standard{indGroup} = find(outliers==0);

        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:2
            for j = 1:1
                data{i, j} = dataToPlot(:,i);
                % individually demean for within-subject visualization
                data_ws{i, j} = dataToPlot(:,i)-...
                    nanmean(dataToPlot(:,:),2)+...
                    repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
                data_nooutlier{i, j} = data{i, j};
                data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                data_ws_nooutlier{i, j} = data_ws{i, j};
                data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                % sort outliers to back in original data for improved plot overlap
                data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        cla;
        cl = cBrew(indGroup,:);
        [~, spacing] = rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
        h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],spacing);
        view([90 -90]);
        axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'mismatch'; 'match'});
        yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(2)+(yticks(2)-yticks(1))./2]);

        minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
        xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
        ylabel('Condition'); xlabel({'Stroop CPP'})

        % test linear effect
        curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}];
        X = [1 1; 1 2]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        M = mean(IndividualSlopes);    
        if M>10^-3; tit_m = ['M: ', num2str(round(M,2))]; else; tit_m = sprintf('M: = %.1e',M); end
        if p>10^-3; tit_p = ['p: ', num2str(round(p,2))]; else; tit_p = sprintf('p = %.1e',p); end
        title([tit_m, ',', tit_p])
end
set(findall(gcf,'-property','FontSize'),'FontSize',20)
figureName = 'B_StroopCPP_bymatch';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot as bar plots

colorm = [.6 .6 .6; 1 .6 .6; .6 .8 1];

h = figure('units','centimeters','position',[.1 .1 9 9]);
set(gcf,'renderer','Painters')
plot_data{1} = slopes{1}(:,2,1)-slopes{1}(:,1,1);
plot_data{2} = slopes{2}(:,2,1)-slopes{2}(:,1,1);
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:2
    if ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})>0
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(2,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    elseif ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})<0
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(3,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    elseif ttest(plot_data{indGroup})==0
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    end
    % plot individual values on top
    scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
        plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 2.75]); %ylim([1.3 2])
set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'});
ylabel({'CPP slope','Mismatch-Match'});
[htest, p] = ttest2(plot_data{1}, plot_data{2});
if p>10^-3
    title(['p = ', num2str(round(p,2))]);
else
    tit = sprintf('p = %.1e',p);
    title(tit);
end
set(findall(gcf,'-property','FontSize'),'FontSize',20)
figureName = 'B_StroopCPP_mismatch';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% save CPP slope estimates

% 2160 removed as S2 is missing 
% 2112 removed due to bad data quality

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'; ...
    '2104';'2107';'2108';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

stroop_cpp.data = cat(1,slopes{1}(:,:,1),slopes{2}(:,:,1));
stroop_cpp.IDs = IDs;

save(fullfile(pn.dataOut, 'stroop_cpp.mat'), 'stroop_cpp');

%% focus on conditions

h = figure('units','normalized','position',[.1 .1 .15 .25]);
set(gcf,'renderer','Painters')
    cla; hold on;
    
    smoothPoints = 15*2; % 60 ms smoothing
    curData = squeeze(nanmean(ERPdata{1}(1,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h1 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(1,:)],'linewidth', 1,...
        'linestyle', '-', 'marker', 'none'}, 'patchSaturation', .25);
    %h1 = plot(time,nanmean(curData,1), 'k-', 'LineWidth', 2);
    curData = squeeze(nanmean(ERPdata{1}(2,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h2 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(1,:)],'linewidth', 1, ...
        'linestyle', '--', 'marker', 'none'}, 'patchSaturation', .25);
    %hold on; h2 = plot(time,nanmean(curData,1),'k--','LineWidth', 2)
    curData = squeeze(nanmean(ERPdata{2}(1,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h3 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(end,:)],'linewidth', 1,...
        'linestyle', '-', 'marker', 'none'}, 'patchSaturation', .25);
    %hold on; h3 = plot(time,nanmean(curData,1), 'r-', 'LineWidth', 2);
    curData = squeeze(nanmean(ERPdata{2}(2,:,:,54,:),2));
    curData = smoothts(squeeze(curData),'b',smoothPoints);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    h4 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', [cBrew(end,:)],'linewidth', 1, ...
        'linestyle', '--', 'marker', 'none'}, 'patchSaturation', .25);
    %hold on; h4 = plot(time,nanmean(curData,1),'r--','LineWidth', 2)
    hold on; line([0,0],[-2,7], 'Color', 'k','LineWidth', 1, 'LineStyle', ':')
    legend([h1.mainLine, h2.mainLine, h3.mainLine, h4.mainLine], ...
        {'Match YA'; 'Mismatch YA';'Match OA'; 'Mismatch OA'}, ...
        'orientation', 'vertical', 'location', 'NorthWest')
    xlabel('Time (peri-SOT; s)'); ylabel('Amplitude')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('POz (CPP)')
    xlim([-.7 0]); ylim([-1.5 7]);
    
figureName = 'B_StroopCPP_YAOA_bycond';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
