%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/A_TFR/D_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/A_TFR/B_data/A_WaveletOut/']; mkdir(pn.FTout);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% load RT data
RTdata = load(['/Volumes/LNDG-1/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/SummaryData.mat'], 'data');

avgS1 = []; avgS2 = [];
for id = 1:length(IDs)
    for indRun = 1:2
        
        if indRun == 1
            condEEG = 'stroop1';
        elseif indRun == 2
            condEEG = 'stroop2';
        end

        display(['processing ID ' IDs{id}]);

        %% load data

        tmp = [];
        tmp.clock = tic; % set tic for processing duration

        %data.(condEEG) = load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
        data.(condEEG) = load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
        
        %% align to trial-wise RT
        eegtime = data.(condEEG).data.time{1};
        idx_curID = find(ismember(RTdata.data.IDs, IDs(id)));
        RTs = RTdata.data.(['StroopOnset',num2str(indRun)])(:,idx_curID);
        % get overview of available trials in final EEG sample
        curTrials = data.(condEEG).data.cfg.trials;
        RTs = RTs(curTrials);
        % remove any potential NaNs in the RT data
        idx_nan= find(isnan(RTs));
        data.(condEEG).data.trial(idx_nan) = [];
        data.(condEEG).data.time(idx_nan) = [];
        RTs(idx_nan) = [];
        % find closest time point
        [tmp, idx] = min(abs(eegtime-RTs),[],2);
        % lock EEG signals to trial-wise RT
        for indTrial = 1:numel(idx)
            data.(condEEG).data.trial{indTrial} = cat(2, data.(condEEG).data.trial{indTrial}, zeros(64,1000));
            curData = data.(condEEG).data.trial{indTrial}(:,idx(indTrial)-1000:idx(indTrial)+1000);
            data.(condEEG).data.trialRT{indTrial} = curData;
            data.(condEEG).data.time{indTrial} = 0-1000*.002:.002:0+1000*.002;
        end
        data.(condEEG).data.trial = data.(condEEG).data.trialRT; 
        data.(condEEG).data = rmfield(data.(condEEG).data, 'trialRT');
        
        % split according to stroop interference
    
        interference = RTdata.data.(['StroopInterference',num2str(indRun)])(:,idx_curID);
        interference = interference(curTrials);
        interference(idx_nan) = [];
        cfg = [];
        cfg.trials = interference==0;
        avg_i0{indRun,id} = ft_timelockanalysis(cfg, data.(condEEG).data);
        cfg = [];
        cfg.trials = interference==1;
        avg_i1{indRun,id} = ft_timelockanalysis(cfg, data.(condEEG).data);
        
    end
end % ID

%% create grand average across subjects

cfg = [];
avgGrandS1_c0 = ft_timelockgrandaverage(cfg, avg_i0{1,:});
avgGrandS1_c1 = ft_timelockgrandaverage(cfg, avg_i1{1,:});
avgGrandS2_c0 = ft_timelockgrandaverage(cfg, avg_i0{2,:});
avgGrandS2_c1 = ft_timelockgrandaverage(cfg, avg_i1{2,:});

%% save data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
save([pn.dataOut, 'B_StroopERPs_RT_YA.mat'], 'avg*');

%% load data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
load([pn.dataOut, 'B_StroopERPs_RT_YA.mat']);

%% plotting section

figure;
subplot(3,1,1);
    imagesc(avgGrandS1_c0.time,1:64,avgGrandS1_c0.avg)
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    title('Grand average Stroop pre')
subplot(3,1,2);
    imagesc(avgGrandS2_c0.time,1:64,avgGrandS2_c0.avg)
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    title('Grand average Stroop post')
subplot(3,1,3);
    imagesc(avgGrandS2_c0.time,1:64,avgGrandS2_c0.avg-avgGrandS1_c0.avg)
    hold on; line([0,0],[64,0], 'Color', 'w','LineWidth', 2)
    title('Grand average Stroop post-pre')


h = figure('units','normalized','position',[.1 .1 .3 .3]);
subplot(2,3,[1,2]);
    plot(avgGrandS2_c0.time,nanmean(avgGrandS1_c0.avg(1:21,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS2_c0.time,nanmean(avgGrandS2_c0.avg(1:21,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    legend({'Stroop Pre'; 'Stroop Post'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Frontal Grand Average')
    xlim([-1 1])
subplot(2,3,[4,5]);
    plot(avgGrandS2_c0.time,nanmean(avgGrandS1_c0.avg(54,:),1),'LineWidth', 2);
    hold on; plot(avgGrandS2_c0.time,nanmean(avgGrandS2_c0.avg(44:60,:),1),'LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 2)
    legend({'Stroop Pre'; 'Stroop Post'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('Posterior-occiptal Grand Average')
    xlim([-1 1])
subplot(2,3,3);
cfg = [];
cfg.xlim = [-.2 .2];
%cfg.zlim = [-.5 .5];
cfg.layout = 'acticap-64ch-standard2.mat';
%cfg.colorbar = 'yes';
ft_topoplotER(cfg,avgGrandS1_c0); title({'Stroop Pre';'-200:200 ms peri-response'})
subplot(2,3,6);
ft_topoplotER(cfg,avgGrandS2_c0); title({'Stroop Post';'-200:200 ms peri-response'})

% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
% figureName = 'StroopPrePostComparison';
% 
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');


h = figure('units','normalized','position',[.1 .1 .3 .3]);
subplot(2,2,[1,2])
    cla;
    h1 = plot(avgGrandS2_c0.time,nanmean(avgGrandS1_c0.avg(54,:),1), 'k-', 'LineWidth', 2);
    hold on; h2 = plot(avgGrandS2_c0.time,nanmean(avgGrandS1_c1.avg(54,:),1),'k--','LineWidth', 2)
    hold on; h3 = plot(avgGrandS2_c0.time,nanmean(avgGrandS2_c0.avg(54,:),1),'r-','LineWidth', 2)
    hold on; h4 = plot(avgGrandS2_c0.time,nanmean(avgGrandS2_c1.avg(54,:),1),'r--','LineWidth', 2)
    hold on; line([0,0],[-10,10], 'Color', 'k','LineWidth', 1, 'LineStyle', ':')
    legend([h1, h2, h3, h4], {'Pre0'; 'Pre1';'Post0'; 'Post1'}, 'orientation', 'horizontal')
    xlabel('Time (peri-RT; s)'); ylabel('Amplitude')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend('boxoff')
    title('POz (CPP)')
    xlim([-1 1]); ylim([-3 7.5]);
subplot(2,2,3);
    cfg = [];
    cfg.xlim = [-.2 .2];
    %cfg.zlim = [-.5 .5];
    cfg.layout = 'acticap-64ch-standard2.mat';
    %cfg.colorbar = 'yes';
    ft_topoplotER(cfg,avgGrandS1_c0); title({'Stroop Pre';'-200:200 ms peri-response'})
subplot(2,2,4);
    ft_topoplotER(cfg,avgGrandS2_c0); title({'Stroop Post';'-200:200 ms peri-response'})
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/C_figures/';
figureName = 'B_StroopCPP';
saveas(h, [pn.plotFolder, figureName], 'png');

%% create avg data for OASs

% N = 51; 
% 2160 removed as S2 is missing 
% 2112 removed due to bad data quality
IDs = {'2104';'2107';'2108';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

% load RT data
RTdata = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/stroop/A_CARL/B_data/SummaryData.mat'], 'data');

avgS1 = []; avgS2 = [];
for id = 1:length(IDs)
    for indRun = 1:2
        
        if indRun == 1
            condEEG = 'stroop1';
        elseif indRun == 2
            condEEG = 'stroop2';
        end

        display(['processing ID ' IDs{id}]);

        %% load data

        tmp = [];
        tmp.clock = tic; % set tic for processing duration

        %data.(condEEG) = load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
        data.(condEEG) = load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
        
        %% align to trial-wise RT
        eegtime = data.(condEEG).data.time{1};
        idx_curID = find(ismember(RTdata.data.IDs, IDs(id)));
        RTs = RTdata.data.(['StroopOnset',num2str(indRun)])(:,idx_curID);
        % get overview of available trials in final EEG sample
        curTrials = data.(condEEG).data.cfg.trials;
        RTs = RTs(curTrials);
        % remove any RTs below 500 ms
        RTs(RTs<.5) = NaN;
        % remove any potential NaNs in the RT data
        idx_nan= find(isnan(RTs));
        data.(condEEG).data.trial(idx_nan) = [];
        data.(condEEG).data.time(idx_nan) = [];
        RTs(idx_nan) = [];
        % find closest time point
        [tmp, idx] = min(abs(eegtime-RTs),[],2);
        % lock EEG signals to trial-wise RT
        for indTrial = 1:numel(idx)
            data.(condEEG).data.trial{indTrial} = cat(2, data.(condEEG).data.trial{indTrial}, zeros(64,1000));
            curData = data.(condEEG).data.trial{indTrial}(:,idx(indTrial)-1000:idx(indTrial)+1000);
            data.(condEEG).data.trialRT{indTrial} = curData;
            data.(condEEG).data.time{indTrial} = 0-1000*.002:.002:0+1000*.002;
        end
        data.(condEEG).data.trial = data.(condEEG).data.trialRT; 
        data.(condEEG).data = rmfield(data.(condEEG).data, 'trialRT');
        
        % split according to stroop interference
    
        interference = RTdata.data.(['StroopInterference',num2str(indRun)])(:,idx_curID);
        interference = interference(curTrials);
        interference(idx_nan) = [];
        cfg = [];
        cfg.trials = interference==0;
        avg_i0{indRun,id} = ft_timelockanalysis(cfg, data.(condEEG).data);
        cfg = [];
        cfg.trials = interference==1;
        avg_i1{indRun,id} = ft_timelockanalysis(cfg, data.(condEEG).data);
        
    end
end % ID

%% create grand average across subjects

cfg = [];
avgGrandS1_c0 = ft_timelockgrandaverage(cfg, avg_i0{1,:});
avgGrandS1_c1 = ft_timelockgrandaverage(cfg, avg_i1{1,:});
avgGrandS2_c0 = ft_timelockgrandaverage(cfg, avg_i0{2,:});
avgGrandS2_c1 = ft_timelockgrandaverage(cfg, avg_i1{2,:});

%% save data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/B_analyses/C_ERP/B_data/';
save([pn.dataOut, 'B_StroopERPs_RT_OA.mat'], 'avg*');

