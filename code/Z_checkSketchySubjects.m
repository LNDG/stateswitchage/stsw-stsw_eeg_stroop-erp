% Inspect Sketchy IDs

% check magnitude of individual ERPs (across channels)

SketchyIDs = {'2112'; '2121';  '2214'};
SketchyIDsidx = find(ismember(IDs, SketchyIDs));

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];

%% contrast pre-post with all channels

figure;
for id = 1:length(SketchyIDsidx)
    subplot(2,3,id)
    imagesc(avgS1{SketchyIDsidx(id)}.avg); colorbar;
end
for id = 1:length(SketchyIDsidx)
    subplot(2,3,3+id)
    imagesc(avgS2{SketchyIDsidx(id)}.avg); colorbar;
end

%% contrast pre-post with all channels ['normal' subjects]

OKid = [1; 6; 20];

figure;
for id = 1:length(OKid)
    subplot(2,3,id)
    imagesc(avgS1{OKid(id)}.avg); colorbar;
end
for id = 1:length(OKid)
    subplot(2,3,3+id)
    imagesc(avgS2{OKid(id)}.avg); colorbar;
end

%% plot single-trials for these three subjects

for id = 1:length(SketchyIDs)
    for indRun = 1:2
        
        if indRun == 1
            condEEG = 'stroop1';
        elseif indRun == 2
            condEEG = 'stroop2';
        end

        display(['processing ID ' SketchyIDs{id}]);
        
        %% load data

        RawData{id,indRun} = load([pn.dataIn, SketchyIDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art.mat'], 'data');
        cfg = [];
        cfg.baseline = [-1 -.5];
        cfg.channel = 'all';
        cfg.parameter = 'trial';
        [BLData{id,indRun}] = ft_timelockbaseline(cfg, RawData{id,indRun}.data);
        cfg = [];
        IndividualERP{id,indRun} = ft_timelockanalysis(cfg, BLData{id,indRun});
    end
end

figure; imagesc(IndividualERP{1,2}.time,[],IndividualERP{1,2}.avg); colorbar;

figure;
for id = 1:length(SketchyIDs)
    for indRun = 1:2
        hold on; plot(nanmean(IndividualERP{id,indRun}.avg,1));
    end
end

figure;
count = 1;
for id = 1:length(SketchyIDs)
    for indRun = 1:2
        RawDataMerged{id,indRun} = cat(3, RawData{id,indRun}.data.trial{:});
        subplot(2,3,count);
        imagesc(squeeze(nanmean(RawDataMerged{id,indRun}(1:60,:,:),1))'); colorbar;
        count = count+1;
    end
end

figure;
count = 1;
for id = 1:length(SketchyIDs)
    for indRun = 1:2
        RawDataMerged{id,indRun} = cat(3, RawData{id,indRun}.data.trial{:});
        subplot(2,3,count);
        imagesc(squeeze(nanmean(RawDataMerged{id,indRun}(1:64,:,:),3))); colorbar;
        count = count+1;
    end
end

% There seems to be something intrinsically wrong with the data.

figure; imagesc(RawData{id,indRun}.data.trial{1,1})
figure; imagesc(squeeze(BLData{id,indRun}.trial(1,:,:)))

% Let's take a look at the raw data.

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/B_data/C_EEG_FT/2121_stroop1_EEG_Raw.mat')

figure; imagesc(data_eyeEEG.trial{1,1}, [-500 500]); colorbar;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/stroop/A_preproc/SA_preproc_study/B_data/C_EEG_FT/2121_stroop2_EEG_Raw_Rlm_Flh_Res.mat')

figure; imagesc(data_EEG.trial{1,1}, [-500 500]); colorbar;
figure; imagesc(data.trial{1,4}, [-500 500]); colorbar;
figure; imagesc(data.trial{1,1}); colorbar;
