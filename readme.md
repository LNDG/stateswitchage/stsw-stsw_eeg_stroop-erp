[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## EEG Stroop ERP

**Maintainer:** [Julian Q. Kosciessa](kosciessa@mpib-berlin.mpg.de)

## Code overview

**B2_ERP_RTlocked_plot**

![image](figures/B_StroopCPP_YAOA.png)